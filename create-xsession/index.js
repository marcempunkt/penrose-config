const readline = require("readline");
const path = require("path");
const fs = require("fs");
const colors = require("colors");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const askForPermission = `
${colors.red(":: Creating desktop entry for your penrose instance")}
==> Do you want to create an dekstop entry in /usr/share/xsessions/penrose.desktop ${colors.blue("(y/n)")} `;

rl.question(askForPermission, answer => {
  const isYes = answer.match("^(?:Yes|YES|yes|y|Y)$");
  const isNo = answer.match("^(?:No|NO|no|n|N)$");

  if (isYes) {
    console.log("Creating entry...");
    createDesktopEntry();
  } else if (isNo) {
    console.log(colors.yellow("Aborted"));
  } else {
    console.log(colors.red(`Unknown Answer: ${answer}`));
  }

  rl.close();
});

const createDesktopEntry = () => {
  const projectRoot = path.normalize(path.join(__dirname, ".."));
  // project should be called something like: penrose.d
  const projectObject = path.parse(projectRoot); // => { root, dir, base, ext, name }
  const projectBase = projectObject.base; // => penrose.d
  const projectName = projectObject.name; // => penrose

  const desktopFilePath = path.join(__dirname, "penrose.desktop");

  const penroseTemplate = `[Desktop Entry]
Name=${projectName}
Comment=Window Tiling Manager written in Rust
Exec=${path.join(projectRoot, "target", "release", projectName)}
Type=Application`;

  // console.log(colors.yellow(penroseTemplate))
  const xsessionsPenroseDesktopPath = path.join("/usr", "share", "xsessions", "penrose.desktop");

  fs.writeFile(xsessionsPenroseDesktopPath, penroseTemplate, err => {
    if (err) return console.log(colors.red(err));
    console.log(colors.green("Successfully created penrose.desktop in /usr/share/xsessions"));
  });

};

