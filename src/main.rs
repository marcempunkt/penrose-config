#[macro_use]
extern crate penrose;

use penrose::{
    core::{
	helpers::{ index_selectors, spawn },
	layout::{ bottom_stack, monocle, side_stack, Layout, LayoutConf },
    },
    logging_error_handler,
    // for xcb you will need to install c libaries like xcb, Cairo, Pango
    xcb::new_xcb_backed_window_manager,
    Backward,
    Config, // instance which contains the rest of your top level configuration
    Forward,
    Less,
    More,
    WindowManager,
};

// run a bash command and don't care about the error
fn run_bash_command(command: &str) {
    let _ = spawn(command);
} 

fn main() -> penrose::Result<()> {

    let mut config = Config::default();
    // config.workspaces = vec!["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
    /*
    config.fonts = &[
	"SF Pro Display:style=Regular:size=12;1",
	"Noto Sans JP Regular:style=Regular:size=11;2",
	"Noto Sans KR Regular:style=Regular:size=12;2"
    ];
    config.floating_classes = &["rofi", "dmenu", "dunst", "polybar"];
    */

    let key_bindings = gen_keybindings! {
	// M Super
	// S Shift
	// A Control???
	// grave ` or ^° idk 

	// Cycle focus between clients for the active Workspace
	"M-j" => run_internal!(cycle_client, Forward);
	"M-k" => run_internal!(cycle_client, Backward);
	// Move the foucsed [Client] through the stack of clients on the active Workspace.
	"M-S-j" => run_internal!(drag_client, Forward);
	"M-S-k" => run_internal!(drag_client, Backward);
	// Kill Client
	"M-c" => run_internal!(kill_client);
	// Switch to last active Workspace
	"M-w" => run_internal!(toggle_workspace);

	// Alter Client size
    // Ist das BÖSE
	"M-A-j" => run_internal!(update_max_main, More); 
	"M-A-k" => run_internal!(update_max_main, Less); 

	"M-A-h" => run_internal!(update_main_ratio, Less); 
	"M-A-l" => run_internal!(update_main_ratio, More); 
	// Some basic Application Shortcuts
	"M-e" => run_external!("~/.config/rofi/gnome/launcher.sh");
	"M-Tab" => run_external!("rofi -show window");
	"M-S-p" => run_external!("~/.config/polybar/launch.sh");
	"M-Return" => run_external!("alacritty");

	// Update Display
	"M-A-s" => run_internal!(detect_screens);
	// Quit penrose
	"M-A-Escape" => run_internal!(exit);

	// Create Workspaces
	refmap [ 1..10 ] in {
            "M-{}" => focus_workspace [ index_selectors(10) ];
            "M-S-{}" => client_to_workspace [ index_selectors(10) ];
	};
    };
    
    let mut wm = new_xcb_backed_window_manager(Config::default(), vec![], logging_error_handler())?;

    // Startup
    run_bash_command("xsetroot -cursor_name left_ptr");
    run_bash_command("xinput set-prop 11 'libinput Accel Speed' -0.63");
    run_bash_command("xrandr --output DP-0 --mode 2560x1440 --rate 144.00");
    run_bash_command("nitrogen --restore");
    run_bash_command("picom --experimental-backend");
    run_bash_command("emacs --deamon");
    // Systray
    run_bash_command("\\$HOME/.config/polybar/launch.sh");
    run_bash_command("redshift - t 5700:2500 -l 50.11:8.68");
    run_bash_command("fctix");
    run_bash_command("volumeicon");
    run_bash_command("caffeine");
    run_bash_command("nm-applet");
    run_bash_command("blueberry-tray");
    run_bash_command("protonmail-bridge --no-window");
    run_bash_command("/opt/piavpn/bin/pia-client");

    wm.grab_keys_and_run(key_bindings, map! {})
}
